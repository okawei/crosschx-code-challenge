<%--
  Created by IntelliJ IDEA.
  User: maxheckel
  Date: 6/14/15
  Time: 11:38 AM
--%>
<%@page expressionCodec="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Double Agent Locations</title>

    <link rel="stylesheet" href="${resource(dir: 'bower_components', file: 'leaflet/dist/leaflet.css')}">
    <link rel="stylesheet" href="${resource(dir: 'bower_components', file: 'bootstrap/dist/css/bootstrap.min.css')}">
    <link rel="stylesheet" href="${resource(dir: 'stylesheets', file: 'application.css')}">
</head>

<body>

<h1 class="pageTitle">
    Double Agent Locations
</h1>

<div class="searchBarBox">
    <input type="text" class="searchBar" placeholder="Search Names...">
</div>

<div class="ageFilterBox">
    <input type="number" class="ageFilter" placeholder="Max age filter">
</div>

<div id="map"></div>


<script>
    var mapData = ${mapData}
</script>

<script src="${resource(dir: 'javascripts', file: 'jquery-2.1.3.js')}"></script>
<script src="${resource(dir: 'bower_components', file: 'leaflet/dist/leaflet.js')}"></script>
<script src="${resource(dir: 'javascripts', file: 'application.js')}"></script>
</body>
</html>