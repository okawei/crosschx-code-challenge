package crosschxchallenge2

import groovy.json.JsonOutput

class MapController {

    def index() {

        def mapData = this.getMapData()
        render(view: "mapTemplate", model: ['mapData':mapData])

    }

    private def getMapData(){
        def f = new File('cc-maps-data-set.csv')
        if(!f.exists()){
            render '{error: "ERR-MISSING-FILE"}'
        }

        def lines = f.readLines()
        def keys = ['name', 'latitude', 'longitude', 'age', 'sex']
        def rows = lines[1..-1].collect { line ->
            def i = 0, vals = line.split(',')
            keys.inject([:]) { map, key -> map << ["$key": vals[i++]] }
        }
        return JsonOutput.toJson(rows);
    }

}
