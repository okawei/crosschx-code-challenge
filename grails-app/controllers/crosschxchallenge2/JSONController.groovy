package crosschxchallenge2

import groovy.json.JsonOutput

class JSONController {


    private def getRowsArray(){
        def f = new File('cc-maps-data-set.csv')
        if(!f.exists()){
            render '{error: "ERR-MISSING-FILE"}'
        }

        def lines = f.readLines()
        def keys = ['name', 'latitude', 'longitude', 'age', 'sex']
        def rows = lines[1..-1].collect { line ->
            def i = 0, vals = line.split(',')
            keys.inject([:]) { map, key -> map << ["$key": vals[i++]] }
        }

        return rows
    }


    /**
     * We would use this to get the csv data in JSON format via an AJAX request
     * @return
     */
    def index() {

        def rows = this.getRowsArray()
        header 'Content-Type', "Application/json"

        render (JsonOutput.toJson(rows))
    }


    /**
     * Outputs a JSON string for all values with the provided name
     * @return
     */
    def agents(){
        def rows = this.getRowsArray()
        rows = rows.findAll{
            List<List<String>> l = new ArrayList<List<String>>(it.values());
            l.get(0) == params.name
        }
        header 'Content-Type', "Application/json"

        render (JsonOutput.toJson(rows))

    }

    /**
     * Outputs a JSON string for all values who's age is greater than a provided age
     */
    def ages(){
        int maxAge = Integer.parseInt(params.maxAge)
        def rows = this.getRowsArray()
        rows = rows.findAll{
            List<List<String>> l = new ArrayList<List<String>>(it.values());
            Integer.parseInt(l.get(3)) <= maxAge

        }
        header 'Content-Type', "Application/json"

        render (JsonOutput.toJson(rows))

    }
}
