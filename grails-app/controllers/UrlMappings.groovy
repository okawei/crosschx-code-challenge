class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'Map', action: 'index')
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/JSON/agents/$name"(controller:"JSON", action: "agents")
        "/JSON/ages/$maxAge"(controller:"JSON", action: "ages")
    }
}
