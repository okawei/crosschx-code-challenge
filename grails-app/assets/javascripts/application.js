var mapData = null;

$.ajax({
    method: "GET",
    url: "/JSON"
}).done(function( data ) {
    mapData = data;
    addAllMarkers()
});

//Setup the map
var map = L.map('map', { zoomControl:false }).setView([51.505, -0.09], 2);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 13,
    id: 'okawei.904bef97',
    accessToken: 'pk.eyJ1Ijoib2thd2VpIiwiYSI6IjMwMmYzZDYxMmFiZThkZDI1ZTdjMjViZGE3YWQxYjUzIn0.6pMMzvGzCdOedbva64fXAQ'
}).addTo(map);


//Add the icons we need
var femaleIcon = L.icon({
    iconUrl: '/assets/female.png',
    iconRetinaUrl: '/assets/female@2x.png',
    shadowUrl: '/assets/leaflet/dist/images/marker-shadow.png',
    shadowRetinaUrl: '/assets/leaflet/dist/images/marker-shadow.png',
});
var maleIcon = L.icon({
    iconUrl: '/assets/male.png',
    iconRetinaUrl: '/assets/male@2x.png',
    shadowUrl: '/assets/leaflet/dist/images/marker-shadow.png',
    shadowRetinaUrl: '/assets/leaflet/dist/images/marker-shadow.png',
});
var highlightedIcon = L.icon({
    iconUrl: '/assets/highlighted.png',
    iconRetinaUrl: '/assets/highlighted@2x.png',
    shadowUrl: '/assets/leaflet/dist/images/marker-shadow.png',
    shadowRetinaUrl: '/assets/leaflet/dist/images/marker-shadow.png',
});

var markers = [];

function getMarkerForAgent(agent){
    var iconToSet = femaleIcon;
    //Add the agents to the map
    if(agent.sex == 'Male'){
        iconToSet = maleIcon;
    }

    var marker = new L.marker([agent.latitude, agent.longitude], {
        icon: iconToSet
    });

    marker.bindPopup("<b class='name' data-name='"+agent.name+"'>"+agent.name+"</b><br><b>Age: </b>"+agent.age+"<br><b class='sex' data-sex='"+agent.sex+"'>Sex: </b>"+agent.sex);
    return marker;
}

function removeAllMarkers(){
    //Remove all markers currently on the map
    markers.forEach(function(value){
        map.removeLayer(value);
    })
    markers = [];
}

function addAllMarkers() {

    var totalLng = 0, totalLat = 0;
    mapData.forEach(function (agent, index) {

        //Add up the totals to find the center point of the agents
        totalLat = parseFloat(totalLat) + parseFloat(agent.latitude);
        totalLng = parseFloat(totalLng) + parseFloat(agent.longitude);

        var marker = getMarkerForAgent(agent);
        markers.push(marker);

        map.addLayer(markers[index]);
    });

//Calculate the center point of the agents and set the map to that point
    var centerLng = totalLng / mapData.length;
    var centerLat = totalLat / mapData.length;
    map.setView([centerLat, centerLng], 4);

}
//Filter the age
$('.ageFilter').keyup(function(){

    removeAllMarkers();
    //Check to see if the input is a number, if not return
    var age = parseInt($(this).val());
    if(isNaN(age) || age < 1){
        $.ajax({
            method: "GET",
            url: "/JSON"
        }).done(function( data ) {
            mapData = data;
            addAllMarkers()
        });
        return;
    }


    $.ajax({
        method: "GET",
        url: "/JSON/ages/"+age
    }).done(function( data ) {
        mapData = data;
        addAllMarkers()
    });


});

$('.searchBar').keyup(function(){

    var name = $(this).val();


    markers.forEach(function(marker){

        //Unset any currently highlighted markers
        if(marker._icon.currentSrc.indexOf('highlighted.png') > -1){
            if($($(marker._popup._content)[5]).attr('data-sex') == 'Male'){
                marker.setIcon(maleIcon)
            } else{
                marker.setIcon(femaleIcon);
            }
        }

        //If this is the marker we're looking for, highlight it.
        if($($(marker._popup._content)[0]).attr('data-name') == name){
            marker.setIcon(highlightedIcon);
        }


    })





})

